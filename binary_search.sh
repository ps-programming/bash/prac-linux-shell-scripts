set -A arr 1 2 3 4 5
echo "ENTER ELEMENT TO SEARCH"
read e
li=0
ui=4
while [[ $ui -ge 0 -a $li -le 4 ]]; do
	mid=$(( (li + ui) / 2 ))
	if [[ ${arr[mid]} -eq e ]]; then
		echo "ELEMENT FOUND AT $mid"
		break
	elif [[ ${arr[mid]} -le e ]]; then
		li=mid
	else
		ui=mid
	fi
done